import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/services.dart';
import 'dart:async' show Future;
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: const Color(0xFF263238),
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 1;
  int _score = 0;
  String _currentQuestion = '';
  bool _currentAnswer = false;
  List _questions = [];
  List _alreadyAnswered = [];

  void _newQuestion() {
    int currentidx = new Random().nextInt(10);
    if (_counter > 10) {
      this._displayScore();
    } else {
      while (_alreadyAnswered.contains(currentidx)) {
        currentidx = new Random().nextInt(10);
      }
      setState(() {
        _currentQuestion = _questions[currentidx]['question'];
        _currentAnswer = _questions[currentidx]['answer'];
        _alreadyAnswered.add(currentidx);
      });
      // print(_questions);
      print(_questions[currentidx]['question']);
      print(_questions[currentidx]['answer']);
    }
  }

  void _pressTrue() {
    setState(() {
      _counter += 1;
      if (_currentAnswer == true) {
        _score += 1;
      }
    });
    this._newQuestion();
  }

  void _pressFalse() {
    setState(() {
      _counter += 1;
      if (_currentAnswer == false) {
        _score += 1;
      }
    });
    this._newQuestion();
  }

  Future<void> _displayScore() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Score'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Your score is : $_score'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Thanks'),
              onPressed: () {
                setState(() {
                  _counter = 1;
                  _score = 0;
                  _alreadyAnswered = [];
                  this._newQuestion();
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // Fetch content from the json file
  Future<void> localJsonData() async {
    final String response = await rootBundle.loadString('assets/qna.json');
    final data = await json.decode(response);
    setState(() {
      _questions = data;
      this._newQuestion();
    });
    print(response);
  }

  @override
  void initState() {
    super.initState();
    this.localJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'La Grece Antique',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 18
              ),
            ),
            Text(
              '$_counter / 10',
              style: TextStyle(
                color: Colors.grey
              ),
            ),
            Text(
              '$_currentQuestion',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
                ),
            ),
            // Text(
            //   '$_currentAnswer',
            //   style: Theme.of(context).textTheme.headline6,
            // ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: Stack(
                      children: <Widget>[
                        Positioned.fill(
                          child: Container(
                            color: Colors.green,
                          ),
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            padding: const EdgeInsets.all(16.0),
                            primary: Colors.white,
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: _pressTrue,
                          child: const Text('True'),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 30),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: Stack(
                      children: <Widget>[
                        Positioned.fill(
                          child: Container(
                            color: Colors.red,
                          ),
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            padding: const EdgeInsets.all(16.0),
                            primary: Colors.white,
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: _pressFalse,
                          child: const Text('False'),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
